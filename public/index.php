<?php

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

require('../bootstrap.php');
$config = require('../config.php');

$access_token = $config['access_token'];

$args = [
    "token" => $config['token'],
    "url" => $config['url'],
];

if ($_GET['set_webhook']) {
    $core = new \App\Telegram\Bot($args);

    echo print_r($core->setWebhook([
        'url' => 'https://telegram.bot.acrossoffwest.ru?access_token='.$access_token.'&test=test'
    ]), true);
    die();
}
if(
    empty($_GET["access_token"])
    || $_GET["access_token"] != $access_token
    || file_get_contents("php://input") == ""
) {
    echo '404';
    return http_response_code(404);
}

$data = json_decode(file_get_contents("php://input"));
$v = file_get_contents("php://input");
$v = print_r($v, true);
$varsSM = null;
$core = new \App\Telegram\Bot($args);
if(!empty($data->message->text)) {
    switch($data->message->text){
        case "спасибо":
            $varsSM = [
                "name" => "sendMessage",
                "chat_id" => $data->message->chat->id,
                "text" => "Не за что",
                "reply_markup" => '{"keyboard": [["Здарова"], ["Ты кто такой?"]],"one_time_keyboard": true}'
            ];
            break;
        case "Сайн байна":
            $varsSM = [
                "name" => "sendMessage",
                "chat_id" => $data->message->chat->id,
                "text" => $data->message->from->first_name.' ши юу хэнэбшэ эндэ?',
                "reply_markup" => '{"keyboard": [["Ухаа мудажа байнаб, а ши юу хэжэ байна?"]],"one_time_keyboard": true}'
            ];
            break;
        case "Ухаа мудажа байнаб, а ши юу хэжэ байна?":
            $varsSM = [
                "name" => "sendMessage",
                "chat_id" => $data->message->chat->id,
                "text" => "А юу хэхэбтээ унтахамни",
                "reply_markup" => '{"keyboard": [["Зай, тонилоо"]],"one_time_keyboard": true}'
            ];
            break;
        case "Зай, тонилоо":
            $varsSM = [
                "name" => "sendMessage",
                "chat_id" => $data->message->chat->id,
                "text" => "Ништяк, давай удачи братуха борцуха",
                "reply_markup"
            ];
            break;
        default:
            $varsSM = [
                "name" => "sendMessage",
                "chat_id" => $data->message->chat->id,
                "text" => "Амар мэндээ",
                "reply_markup" => '{"keyboard": [["Сайн байна"]],"one_time_keyboard": true}'
            ];
            break;
    }
}

$core->sendMessage($varsSM);
