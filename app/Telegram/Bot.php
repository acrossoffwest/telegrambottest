<?php

namespace App\Telegram;

class Bot
{
    private $token;
    private $requiredArgs = [
        "token",
        "url"
    ];

    public $methods = [
        "sendMessage" => [
            "name" => "",
            "args" => [
                "name",
                "chat_id",
                "text",
                "reply_markup"
            ]
        ],
        'setWebhook' => [
            'name' => 'Set WebHook',
            'args' => [
                'url'
            ]
        ]
    ];

    private $errors = [
        0 => "Args not passed verify.",
        1 => "Bot not found, please check init data for TelegramBot."
    ];

    public $url;

    public $profile = null;

    function __construct ($args)
    {
        if(!$this->verify($args, $this->requiredArgs))
            throw new Exception($this->errors[0]);
        $this->url = $args["url"].$args["token"]."/";
        $result = json_decode($this->getMe());
        if($result->ok != 1)
            throw new Exception($this->errors[1]);
        $this->profile = $result->result;
    }

    private function verify ($args, $requiredArgs)
    {
        if(count($requiredArgs) != count($args))
            return false;
        foreach($args as $key => $value){
            if(!in_array($key, $requiredArgs) || $value == "")
                return false;
        }
        return true;
    }

    public function getMethodUrl ($args)
    {
        $requiredArgs = [
            "name"
        ];
        if(!$this->verify($args, $requiredArgs))
            throw new Exception($this->errors[0]);
        return $this->url.$args["name"];
    }

    public function doMethod ($args)
    {
        $data = empty($args["data"]) ? "" : $args["data"];
        return file_get_contents($this->getMethodUrl(["name" => $args["name"]]).$data);
    }

    public function getMe ()
    {
        $curMethod = explode('::', __METHOD__)[1];
        return $this->doMethod(["name" => $curMethod]);
    }

    public function sendMessage ($args)
    {
        $curMethod = explode('::', __METHOD__)[1];
        if(!$this->verify($args, $this->methods[$curMethod]["args"]))
            throw new Exception($this->errors[0]);
        $data = "?chat_id=".$args["chat_id"]."&text=".$args["text"]."&reply_markup=".$args["reply_markup"];
        return $this->doMethod(["name" => $curMethod, "data" => $data]);
    }

    public function setWebhook ($args)
    {
        $curMethod = explode('::', __METHOD__)[1];
        if(!$this->verify($args, $this->methods[$curMethod]["args"]))
            throw new Exception($this->errors[0]);
        $data = "?url=".$args['url'];
        return $this->doMethod(["name" => $curMethod, "data" => $data]);
    }

    public function getUpdates ()
    {
        $curMethod = explode($this->mD, __METHOD__)[1];
        return $this->doMethod(["name" => $curMethod]);
    }

    public function getWeather ($args, $token = "8d4908ea6a28c5f2")
    {
        $requiredArgs = [
            "latitude",
            "longitude"
        ];

        if(!$this->verify($args, $requiredArgs))
            throw new Exception($this->errors[0]);
        $url = "http://api.wunderground.com/api/".$token."/geolookup/conditions/lang:RU/q/".$args["latitude"].",".$args["longitude"].".json";
        return file_get_contents($url);
    }
}
