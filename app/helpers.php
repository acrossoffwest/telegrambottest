<?php

if (!function_exists('rus2translit')) {
    function rus2translit($string) {
        $converter = array(
            'а' => 'a',   'б' => 'b',   'в' => 'v',
            'г' => 'g',   'д' => 'd',   'е' => 'e',
            'ё' => 'e',   'ж' => 'zh',  'з' => 'z',
            'и' => 'i',   'й' => 'y',   'к' => 'k',
            'л' => 'l',   'м' => 'm',   'н' => 'n',
            'о' => 'o',   'п' => 'p',   'р' => 'r',
            'с' => 's',   'т' => 't',   'у' => 'u',
            'ф' => 'f',   'х' => 'h',   'ц' => 'c',
            'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sch',
            'ь' => '',  'ы' => 'y',   'ъ' => '',
            'э' => 'e',   'ю' => 'yu',  'я' => 'ya',

            'А' => 'A',   'Б' => 'B',   'В' => 'V',
            'Г' => 'G',   'Д' => 'D',   'Е' => 'E',
            'Ё' => 'E',   'Ж' => 'Zh',  'З' => 'Z',
            'И' => 'I',   'Й' => 'Y',   'К' => 'K',
            'Л' => 'L',   'М' => 'M',   'Н' => 'N',
            'О' => 'O',   'П' => 'P',   'Р' => 'R',
            'С' => 'S',   'Т' => 'T',   'У' => 'U',
            'Ф' => 'F',   'Х' => 'H',   'Ц' => 'C',
            'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Sch',
            'Ь' => '',  'Ы' => 'Y',   'Ъ' => '',
            'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',
        );
        return strtr($string, $converter);
    }
}

if (!function_exists('full_url_')) {
    function full_url_()
    {
        return "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    }
}

if (!function_exists('plural_form')) {
    function plural_form($number, $after) {
      $cases = array (2, 0, 1, 1, 1, 2);
      return $after[ ($number%100>4 && $number%100<20)? 2: $cases[min($number%10, 5)] ];
    }
}

if (!function_exists('get_url_contents_and_final_url')) {
    function get_url_contents_and_final_url(&$url)
    {
        do
        {
            $context = stream_context_create(
                [
                    "http" => [
                        "follow_location" => false,
                    ]
                ]
            );

            $result = file_get_contents($url, false, $context);

            $pattern = "/^Location:\s*(.*)$/i";
            $location_headers = preg_grep($pattern, $http_response_header);

            if (!empty($location_headers) &&
                preg_match($pattern, array_values($location_headers)[0], $matches))
            {
                $url = $matches[1];
                $repeat = true;
            }
            else
            {
                $repeat = false;
            }
        }
        while ($repeat);

        return $result;
    }
}

if (!function_exists('str_replace_by_pattern')) {
    function str_replace_by_pattern($string, $params)
    {
        foreach ($params as $k => $v) {
            $string = str_replace('{' . $k . '}', $v, $string);
        }
        return $string;
    }
}

if (!function_exists('social_share_link')) {
    function social_share_link($socialNetworkName, $params)
    {
        $urls = [
            'vkontakte' => 'http://vk.com/share.php?url={url}&title={title}&description={description}&image=&utm_source=vkontakte',
            'facebook' => 'https://www.facebook.com/sharer.php?src=sp&u={url}&utm_source=facebook&caption={title}&description={description}',
            'twitter' => 'https://twitter.com/intent/tweet?text={title}&url={url}&hashtags={hashtags}&utm_source=twitter',
            'odnoklassniki' => 'https://connect.ok.ru/offer?url={url}&title={title}&description={description}',
        ];

        if (empty($urls[$socialNetworkName]) || empty($params)) {
            return null;
        }

        $url = str_replace_by_pattern($urls[$socialNetworkName], $params);
        return $url;
    }
}

if (!function_exists('findInModelsArray')) {
    function findInModelsArray($array, $byColumn, $columnValue)
    {
        foreach ($array as $item) {
            if ($item->{$byColumn} == $columnValue) {
                return true;
            }
        }
        return false;
    }
}

if (!function_exists('convertModelsToUnique')) {
    function convertModelsToUnique($array, $byColumn)
    {
        $result = [];
        foreach ($array as $item) {
            if (!findInModelsArray($result, $byColumn, $item->{$byColumn})) {
                $result[] = $item;
            }
        }
        return $result;
    }
}

if (!function_exists('redirects')) {
    function redirects()
    {
        $redirectUri = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : null;
        $redirectUri = strrpos($redirectUri, '?') == (strlen($redirectUri) - 1) ? str_replace('?', '', $redirectUri) : $redirectUri;

        $isNeedRedirect = !preg_match('/\/$/', $redirectUri)
            && !preg_match('/\.html$/', $redirectUri)
            && !preg_match('/\.xml$/', $redirectUri)
            && !strrpos($redirectUri, '?')
            && !strrpos($redirectUri, '#')
            && (isset($_SERVER['REQUEST_URI']) && $_SERVER['REQUEST_METHOD'] != 'POST');
        if ($isNeedRedirect) {
            header("HTTP/1.1 301 Moved Permanently");
            header('Location: '.$redirectUri.'/');
            die($redirectUri);
        }
    }
}

if (!function_exists('special_route')) {
    function special_route($url)
    {
        $redirectUri = $url;
        $redirectUri = strrpos($redirectUri, '?') == (strlen($redirectUri) - 1) ? str_replace('?', '', $redirectUri) : $redirectUri;

        $isNeedRedirect = !preg_match('/\/$/', $redirectUri)
            && !preg_match('/\.html$/', $redirectUri)
            && !strrpos($redirectUri, '?')
            && !strrpos($redirectUri, '#');

        if ($isNeedRedirect) {
            return $redirectUri.'/';
        }

        return $redirectUri;
    }
}

if (!function_exists('end_url')) {
    function end_url()
    {
        $redirectUri = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : null;
        if (empty($redirectUri)) {
            return false;
        }

        $redirectUri = strrpos($redirectUri, '?') == (strlen($redirectUri) - 1) ? str_replace('?', '', $redirectUri) : $redirectUri;

        $isEndUrl = preg_match('/\.html/', $redirectUri);

        if ($isEndUrl) {
            return true;
        }

        return false;
    }
}

if (!function_exists('ruStrToAcronym')) {
    function ruStrToAcronym($str)
    {
        $result = null;

        if(preg_match_all('/\b([^0-9{Cyrillic}])/u',strtolower($str),$m)) {
            $result = mb_strtolower(str_replace(' ', '', implode('',$m[1])));
        }

        return $result;
    }
}

if (!function_exists('appendParamUrl')) {
    function appendParamUrl($url, $params)
    {
        if (empty($url)) {
            $url = full_url_();
        }
        $parts = parse_url($url);
        $queryParams = array();
        if (!empty($parts['query'])) {
            parse_str($parts['query'], $queryParams);
        }
        foreach ($params as $k => $v) {
            $queryParams[$k] = $v;
        }
        $queryString = http_build_query($queryParams);
        if (strlen($queryString))
            return $parts['path'] . '?' . $queryString;
        else
            return $parts['path'];
    }
}

if (!function_exists('removeParamInUrl')) {
    function removeParamInUrl($url, $names)
    {
        if ($url == null) $url = full_url_();
        $parts = parse_url($url);
        $queryParams = array();
        if (!empty($parts['query'])) {
            parse_str($parts['query'], $queryParams);
        }
        foreach ($names as $name) {
            unset($queryParams[$name]);
        }
        $queryString = http_build_query($queryParams);
        if (strlen($queryString))
            return $parts['path'] . '?' . $queryString;
        else
            return $parts['path'];
    }
}

if (!function_exists('StringIsHas')) {
    function StringIsHas($string, $stringSecond, $obj)
    {
        return $obj?$string:$stringSecond;
    }
}

if (!function_exists('dd')) {
    function dd ($obj)
    {
        echo '<pre>'.print_r($obj, true).'</pre>';
    }
}
